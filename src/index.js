// Implement the body of the function below & ensure it passes the provided unit tests by running npm run test.
const sum = (arrayOfNumbers) => {
  var totalOfArrayOfNumbers = 0;
  for (iteration = 0; iteration < arrayOfNumbers.length; iteration++){
    totalOfArrayOfNumbers += arrayOfNumbers[iteration];
  }
  return totalOfArrayOfNumbers;
};

module.exports = {
  sum,
};
